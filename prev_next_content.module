<?php

/**
 * Implements hook_help().
 */
function prev_next_content_help($path, $arg) {
  switch ($path) {
    case 'admin/help#prev_next_content':
      return '<h3>Uses:</h3>>
        <p>1.Enable content types which should display "Prev | Next" links at："admin/config/user-interface/prev_next_content_settings".</p>
        <p>2.Find a position to output the $prev_next_content variable in node.tpl.php, the code example pls see in module: README.txt.</p>';
  }
}

/**
 * Implementation of hook_menu().
 */
function prev_next_content_menu() {
  $items['admin/config/user-interface/prev_next_content_settings'] = array(
    'title' => 'Administer prev_next_content',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('prev_next_content_settings_form'),
    'access arguments' => array('administer prev_next_content'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implementation of hook_permission().
 */
function prev_next_content_permission() {
  $perms = array(
    'administer prev_next_content' => array(
      'title' => t('Administer prev_next_content'),
    ),
  );

  return $perms;
}

/**
 * Form callback of page callback of menu - 'admin/config/user-interface/prev_next_content_settings'.
 */
function prev_next_content_settings_form() {
  $enabled_types = variable_get('prev_next_content_enabled_types', array());

  $node_types = node_type_get_names();

  $form['prev_next_content_enabled_types'] = array(
    '#title' => 'Select content types which should display "prev | next" links',
    '#type' => 'checkboxes',
    '#options' => $node_types,
    '#default_value' => $enabled_types ? $enabled_types : array(),
  );

  return system_settings_form($form);
}

/**
 * Support a variable $prev_next for using of template - 'node.tpl'.
 */
function prev_next_content_preprocess_node(&$variables) {
  $node = $variables['node'];
  $enabled_types = variable_get('prev_next_content_enabled_types', array());

  if ($variables['page'] && isset($enabled_types[$node->type])) {
    $variables['prev_next_content'] = previous_next_content($node->nid, $node->type);
  }
}

/**
 * Return html content of $prev_next_content.
 */
function previous_next_content($nid, $type) {
  $output = '';
  
  $sql_previous = "SELECT n.nid, n.title FROM {node} n WHERE n.status = 1 AND n.nid < :nid AND n.type = :type ORDER BY n.nid DESC";
  $result_previous = db_query_range($sql_previous, 0, 1, array(':nid' => $nid, ':type' => $type));

  $sql_next = "SELECT n.nid, n.title FROM {node} n WHERE n.status = 1 AND n.nid > :nid AND n.type = :type ORDER BY n.nid ASC";
  $result_next = db_query_range($sql_next, 0, 1, array(':nid' => $nid, ':type' => $type));

  $prevous = $result_previous->fetch();
  $next = $result_next->fetch();
  
  $output .= '<div class="previous-next">';
  
  /******** Prev ********/
  $output .= '<span class="prev">' . t('Prev: ') . '</span>';

  if ($prevous) {
    $output .= l($prevous->title, 'node/' . $prevous->nid);
  }
  else {
    $output .= '<span class="no-more-content">' . t('none') . '</span>';
  }
  
  /******** Separator ********/
  $output .= ' | ';
  
  /******** Next ********/
  $output .= '<span class="next">' . t('Next: ') . '</span>';

  if ($next) {
    $output .= l($next->title, 'node/' . $next->nid);
  }
  else {
    $output .= '<span class="no-more-content">' . t('none') . '</span>';
  }

  $output .= '</div>';

  return $output;
}
