Uses:

1.Enable content types which should display "Prev | Next" links at："admin/config/user-interface/prev_next_content_settings".

2.Find a position to output the $prev_next_content variable in node.tpl.php:

  <!-- Prev next content -->
  <?php if (isset($prev_next_content)): ?>
    <?php print $prev_next_content; ?>
  <?php endif; ?>
